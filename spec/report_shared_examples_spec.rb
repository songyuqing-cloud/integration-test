require 'rspec'
require 'json'

require_relative '../lib/gitlab_secure/integration_test/shared_examples/report_shared_examples'

RSpec.describe "report shared examples" do
  let(:report) do
    JSON.parse(File.read(File.expand_path(report_path, __dir__)))
  end

  context "with Dependency Scanning report" do
    let(:report_path) { "fixtures/go-modules/gl-dependency-scanning-report.json" }

    it_behaves_like "non-empty report", ["go.sum"]
    it_behaves_like "recorded report" do
      let(:recorded_report) { report }
    end

    it_behaves_like "valid report"
  end

  context "with empty Dependency Scanning report" do
    let(:report_path) { "fixtures/no-vulnerabilities/gl-dependency-scanning-report.json" }

    it_behaves_like "empty report"
    it_behaves_like "recorded report" do
      let(:recorded_report) { report }
    end

    it_behaves_like "valid report"
  end

  context "with SAST report" do
    let(:report_path) { "fixtures/go-modules/gl-sast-report.json" }

    it_behaves_like "non-empty report", ["main.go"]
    it_behaves_like "recorded report" do
      let(:recorded_report) { report }
    end

    it_behaves_like "valid report"
  end

  context "with Secret Detection report" do
    let(:report_path) { "fixtures/secrets/gl-secret-detection-report.json" }

    it_behaves_like "non-empty report", ["id_rsa", "keys/aws", "keys/dsa_key", "keys/ecdsa_key", "keys/ed25519_key", "keys/pgp_ecc_key", "keys/pgp_rsa_key", "keys/rsa_key", "keys/slack_key", "keys/stripe_key", "keys/testkeys-110044-23ecb974342473e425f395ddd5f8f787ac6be90d.json", "keys/twilio_key", "main.php"]
    it_behaves_like "recorded report" do
      let(:recorded_report) { report }
    end

    it_behaves_like "valid report"
  end
end
