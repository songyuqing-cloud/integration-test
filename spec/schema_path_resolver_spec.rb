require 'json'

require_relative '../lib/gitlab_secure/integration_test/schema_path_resolver'

describe GitlabSecure::IntegrationTest::SchemaPathResolver do
  subject { described_class.new(report).path }

  describe "path" do
    context "with SAST report v3.0.0" do
      let(:report) do
        JSON.parse <<-HERE
{
  "version": "3.0.0",
  "scan": {
    "type": "sast"
  }
}
        HERE
      end

      context "when SECURITY_REPORT_SCHEMAS_DIR is set" do
        before do
          ENV["SECURITY_REPORT_SCHEMAS_DIR"] = "custom"
        end

        subject { described_class.new(report).path }

        specify { expect(subject).to eql "custom/v3.0.0/sast-report-format.json" }
      end

      context "when SECURITY_REPORT_SCHEMAS_DIR is NOT set" do
        before do
          ENV["SECURITY_REPORT_SCHEMAS_DIR"] = nil
        end

        subject { described_class.new(report).path }

        specify { expect(subject).to eql "security-report-schemas/v3.0.0/sast-report-format.json" }
      end
    end

    context "with Dependency Scanning report v14.0.0" do
      let(:report) do
        JSON.parse <<-HERE
{
  "version": "14.0.0",
  "scan": {
    "type": "dependency_scanning"
  }
}
        HERE
      end

      specify { expect(subject).to end_with "v14.0.0/dependency-scanning-report-format.json" }
    end
  end
end
