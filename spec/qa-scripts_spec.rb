require 'rspec'

RSpec.shared_examples_for "successful rspec run" do
  it "reports no failures and no errors" do
    expect(subject).to match(/\d+ examples, 0 failures\n/)
  end
end

RSpec.shared_examples_for "failed rspec run" do
  it "reports failures but no errors" do
    output = subject
    expect(subject).to match(/\d+ examples, \d+ failures?\n/)
    expect(subject).not_to match(", 0 failures")
  end
end

RSpec.describe "QA script" do
  def fixtures_path(relative)
    File.join(File.expand_path("../spec/fixtures", __dir__), relative)
  end

  def fixtures_url(relative)
    project_url = ENV.fetch("CI_PROJECT_URL", "https://gitlab.com/gitlab-org/security-products/analyzers/integration-test")
    File.join(project_url, "/-/raw/main/spec/fixtures", relative)
  end

  subject { `#{File.expand_path("../scripts/" + script, __dir__)}` }

  describe "dependency-scanning-qa.rb" do
    let(:script) { "dependency-scanning-qa.rb" }

    context "with no path to actual report" do
      before do
        ENV.delete("DS_REPORT_PATH")
      end

      context "with expected filename" do
        before do
          ENV["DEPENDENCY_SCANNING_REPORT"] = "special-report.json"
        end

        specify { expect(subject).to match(Regexp.new("cannot load actual report.* special-report.json")) }
      end

      context "with no expected filename" do
        before do
          ENV.delete("DEPENDENCY_SCANNING_REPORT")
        end

        specify { expect(subject).to match(Regexp.new("cannot load actual report.* gl-dependency-scanning-report.json")) }
      end
    end

    context "with actual report path" do
      before do
        ENV["DS_REPORT_PATH"] = fixtures_path("go-modules/gl-dependency-scanning-report.json")
      end

      context  "with no expected report URL" do
        before do
          ENV.delete("DS_REPORT_URL")
        end

        context "with expected filename" do
          before do
            ENV["DEPENDENCY_SCANNING_REPORT"] = "special-report.json"
          end

          specify { expect(subject).to match(Regexp.new("cannot load expected report.* qa/expect/special-report.json")) }
        end

        context "with no expected filename" do
          before do
            ENV.delete("DEPENDENCY_SCANNING_REPORT")
          end

          specify { expect(subject).to match(Regexp.new("cannot load expected report.* qa/expect/gl-dependency-scanning-report.json")) }
        end
      end

      context  "with empty expected report URL" do
        before do
          ENV["DS_REPORT_URL"] = ""
        end

        context "with no expected filename" do
          before do
            ENV.delete("DEPENDENCY_SCANNING_REPORT")
          end

          specify { expect(subject).to match(Regexp.new("cannot load expected report.* qa/expect/gl-dependency-scanning-report.json")) }
        end
      end

      context "with expected report URL" do
        context "when reports are the same" do
          before do
            ENV["DS_REPORT_URL"] = fixtures_url("go-modules/gl-dependency-scanning-report.json")
          end

          it_behaves_like "successful rspec run"
        end

        context "when reports are different" do
          before do
            ENV["DS_REPORT_URL"] = fixtures_url("python-pip/gl-dependency-scanning-report.json")
          end

          it_behaves_like "failed rspec run"
        end
      end
    end
  end

  describe "sast-qa.rb" do
    let(:script) { "sast-qa.rb" }

    context "with no path to actual report" do
      before do
        ENV.delete("SAST_REPORT_PATH")
      end

      specify { expect(subject).to match("cannot load actual report") }
    end

    context "with actual report path" do
      before do
        ENV["SAST_REPORT_PATH"] = fixtures_path("go-modules/gl-sast-report.json")
      end

      context  "with no expected report URL" do
        before do
          ENV.delete("SAST_REPORT_URL")
        end

        specify { expect(subject).to match("cannot load expected report") }
      end

      context "with expected report URL" do
        context "when reports are the same" do
          before do
            ENV["SAST_REPORT_URL"] = fixtures_url("go-modules/gl-sast-report.json")
          end

          it_behaves_like "successful rspec run"
        end

        context "when reports are different" do
          before do
            ENV["SAST_REPORT_URL"] = fixtures_url("python-pip/gl-sast-report.json")
          end

          it_behaves_like "failed rspec run"
        end
      end
    end
  end

  describe "secret-detection-qa.rb" do
    let(:script) { "secret-detection-qa.rb" }

    context "with no path to actual report" do
      before do
        ENV.delete("SECRET_DETECTION_REPORT_PATH")
      end

      specify { expect(subject).to match("cannot load actual report") }
    end

    context "with actual report path" do
      before do
        ENV["SECRET_DETECTION_REPORT_PATH"] = fixtures_path("secrets/gl-secret-detection-report.json")
      end

      context  "with no expected report URL" do
        before do
          ENV.delete("SECRET_DETECTION_REPORT_URL")
        end

        specify { expect(subject).to match("cannot load expected report") }
      end

      context "with expected report URL" do
        context "when reports are the same" do
          before do
            ENV["SECRET_DETECTION_REPORT_URL"] = fixtures_url("secrets/gl-secret-detection-report.json")
          end

          it_behaves_like "successful rspec run"
        end

        context "when reports are different" do
          before do
            ENV["SECRET_DETECTION_REPORT_URL"] = fixtures_url("secrets-commits/gl-secret-detection-report.json")
          end

          it_behaves_like "failed rspec run"
        end
      end
    end
  end
end
