require 'rspec'
require 'json'
require 'uri'

require_relative '../lib/gitlab_secure/integration_test/comparable'

RSpec.describe GitlabSecure::IntegrationTest::Comparable do
  describe "scan" do
    subject { described_class.scan input }

    context "with gemnasium scan" do
      let(:input) do
        JSON.parse <<-HERE
  {
    "scanner": {
      "id": "gemnasium",
      "name": "Gemnasium",
      "url": "https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium",
      "vendor": {
        "name": "GitLab"
      },
      "version": "2.29.6"
    },
    "type": "dependency_scanning",
    "start_time": "2021-06-28T15:52:11",
    "end_time": "2021-06-28T15:52:13",
    "status": "success"
  }
        HERE
      end

      it "strips variable fields" do
        expect(subject.dig("scanner", "version")).to be_nil
        expect(subject["start_time"]).to be_nil
        expect(subject["end_time"]).to be_nil
      end

      it "keeps comparable fields" do
        expect(subject.dig("scanner", "id")).not_to be_empty
        expect(subject["type"]).not_to be_empty
        expect(subject["status"]).not_to be_empty
      end

      it "clones input object" do
        expect(input.dig("scanner", "version")).not_to be_empty
        expect(input["start_time"]).not_to be_empty
        expect(input["end_time"]).not_to be_empty
      end
    end

    context "with empty hash" do
      let(:input) { {} }

      specify { expect { subject }.not_to raise_error }
    end
  end

  describe "vulnerabilities" do
    subject { described_class.vulnerabilities input }

    context "with DS vulnerabilities" do
      let(:input) do
        JSON.parse <<-HERE
  [
    {
      "id": "db7b2a4079055a222490d9190394fec297289431b886a3fa4d3dc49ef6b3f0a0",
      "category": "dependency_scanning",
      "name": "Improper Authentication",
      "message": "Improper Authentication in github.com/minio/minio",
      "severity": "High"
    },
    {
      "id": "ce098748d788cdcb3f62771d4a4bcde031f921acc20752f717a5395ba5c2cd0b",
      "category": "dependency_scanning",
      "name": "Allocation of File Descriptors or Handles Without Limits or Throttling",
      "severity": "High"
    },
    {
      "id": "47b962f53335d1657510b0463643516b696f722a1d294858e687a292618ee6d4",
      "category": "dependency_scanning",
      "name": "Concurrent Execution using Shared Resource with Improper Synchronization (Race Condition)",
      "message": "Concurrent Execution using Shared Resource with Improper Synchronization (Race Condition) in github.com/astaxie/beego",
      "severity": "Medium"
    }
  ]
        HERE
      end

      it "sorts objects" do
        hosts = subject.map { |v| v["name"] }
        expect(hosts).to eql [
          "Allocation of File Descriptors or Handles Without Limits or Throttling",
          "Concurrent Execution using Shared Resource with Improper Synchronization (Race Condition)",
          "Improper Authentication"
        ]
      end
    end

    context "with nil" do
      let(:input) { nil }

      specify { expect(subject).to eql [] }
    end
  end

  describe "vulnerability" do
    subject { described_class.vulnerability input }

    context "with DS vulnerability" do
      let(:input) do
        JSON.parse <<-HERE
    {
      "id": "7668de94a30c0b2d10084c0bb23a03bb1c5253a4006743554b8ed6f645639e21",
      "category": "dependency_scanning",
      "name": "Improper Restriction of Operations within the Bounds of a Memory Buffer",
      "message": "Improper Restriction of Operations within the Bounds of a Memory Buffer in Microsoft.ChakraCore",
      "description": "A remote code execution vulnerability exists in the way that the ChakraCore scripting engine handles objects in memory.",
      "cve": "src/web.api/packages.lock.json:Microsoft.ChakraCore:gemnasium:67c33ce7-1101-4dd0-bbd4-b1585409e1c0",
      "severity": "High",
      "solution": "Upgrade to version 1.11.20 or above.",
      "scanner": {
        "id": "gemnasium",
        "name": "Gemnasium"
      },
      "location": {
        "file": "src/web.api/packages.lock.json",
        "dependency": {
          "iid": 31,
          "package": {
            "name": "Microsoft.ChakraCore"
          },
          "version": "1.11.18"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-67c33ce7-1101-4dd0-bbd4-b1585409e1c0",
          "value": "67c33ce7-1101-4dd0-bbd4-b1585409e1c0",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/nuget/Microsoft.ChakraCore/CVE-2020-1073.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2020-1073",
          "value": "CVE-2020-1073",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-1073"
        }
      ],
      "links": [
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2020-1073"
        },
        {
          "url": "https://example.com/vuln/detail/CVE-2020-1073"
        },
        {
          "url": "https://portal.msrc.microsoft.com/en-US/security-guidance/advisory/CVE-2020-1073"
        }
      ]
    }
        HERE
      end

      it "strips variable fields" do
        expect(subject["id"]).to be_nil
        expect(subject.dig("location", "dependency", "iid")).to be_nil
      end

      it "keeps comparable fields" do
        expect(subject["name"]).not_to be_empty
        expect(subject["message"]).not_to be_empty
        expect(subject["description"]).not_to be_empty
        expect(subject["severity"]).not_to be_empty
        expect(subject["solution"]).not_to be_empty
      end

      it "sorts links" do
        hosts = subject["links"].map { |r| URI.parse(r["url"]).host }
        expect(hosts).to eql %w[example.com nvd.nist.gov portal.msrc.microsoft.com]
      end

      it "sorts identifiers" do
        types = subject["identifiers"].map { |identifier| identifier["type"] }
        expect(types).to eql %w[cve gemnasium]
      end
    end

    context "with SAST vulnerability" do
      let(:input) do
        JSON.parse <<-HERE
    {
      "id": "bc71cd43fc55258533d3a11e038d1e179f677d2560da3f7db2ab2d94773a8dec",
      "category": "sast",
      "message": "Improper Check for Unusual or Exceptional Conditions",
      "description": "Use of assert detected. The enclosed code will be removed when compiling to optimised byte code.",
      "cve": "",
      "severity": "Info",
      "scanner": {
        "id": "semgrep",
        "name": "Semgrep"
      },
      "location": {
        "file": "tests/test_simple.py",
        "start_line": 7,
        "end_line": 7
      },
      "identifiers": [
        {
          "type": "semgrep_id",
          "name": "bandit.B101",
          "value": "bandit.B101"
        },
        {
          "type": "cwe",
          "name": "CWE-754",
          "value": "754",
          "url": "https://cwe.mitre.org/data/definitions/754.html"
        },
        {
          "type": "bandit_test_id",
          "name": "Bandit Test ID B101",
          "value": "B101"
        }
      ]
    }
        HERE
      end

      it "strips variable fields" do
        expect(subject["id"]).to be_nil
      end

      it "keeps comparable fields" do
        expect(subject["message"]).not_to be_empty
        expect(subject["description"]).not_to be_empty
        expect(subject["severity"]).not_to be_empty
        expect(subject.dig("location", "file")).not_to be_empty
        expect(subject.dig("location", "start_line")).not_to be_nil
      end
    end

    context "with SD vulnerability" do
      let(:input) do
        JSON.parse <<-HERE
    {
      "id": "a2e7558cd081f5f3ab621b57fc0b6b9fc6e254c5087d3d5560dba39bdbaf0ad4",
      "category": "secret_detection",
      "name": "RSA private key",
      "message": "RSA private key detected; please remove and revoke it if this is a leak.",
      "description": "RSA private key",
      "cve": "id_rsa:8bcac7908eb950419537b91e19adc83ce2c9cbfdacf4f81157fdadfec11f7017:RSA private key",
      "severity": "Critical",
      "confidence": "Unknown",
      "raw_source_code_extract": "-----BEGIN RSA PRIVATE KEY-----",
      "scanner": {
        "id": "gitleaks",
        "name": "Gitleaks"
      },
      "location": {
        "file": "id_rsa",
        "commit": {
          "date": "0001-01-01T00:00:00Z",
          "sha": "0000000"
        },
        "start_line": 1,
        "end_line": 1
      },
      "identifiers": [
        {
          "type": "gitleaks_rule_id",
          "name": "Gitleaks rule ID RSA private key",
          "value": "RSA private key"
        }
      ]
    }
        HERE
      end

      it "strips variable fields" do
        expect(subject["id"]).to be_nil
        expect(subject.dig("location", "commit", "sha")).to be_nil
      end

      it "keeps comparable fields" do
        expect(subject["name"]).not_to be_empty
        expect(subject["message"]).not_to be_empty
        expect(subject["description"]).not_to be_empty
        expect(subject["severity"]).not_to be_empty
        expect(subject.dig("location", "commit", "date")).not_to be_empty
      end
    end

    context "with empty hash" do
      let(:input) { {} }

      specify { expect { subject }.not_to raise_error }

      it "creates missing arrays" do
        expect(subject["links"]).to eql []
        expect(subject.dig("tracking", "items")).to eql []
      end
    end
  end

  describe "dependency_files" do
    subject { described_class.dependency_files input }

    context "with 3 files, 2 filenames" do
      let(:input) do
        JSON.parse <<-HERE
  [
    {
      "path": "yarn.lock",
      "package_manager": "yarn",
      "dependencies": [
        {
          "package": {
            "name": "@babel/code-frame"
          },
          "version": "7.0.0"
        }
      ]
    },
    {
      "path": "yarn.lock",
      "package_manager": "yarn",
      "dependencies": [
        {
          "package": {
            "name": "@babel/core"
          },
          "version": "7.4.4"
        }
      ]
    },
    {
      "path": "package.json",
      "package_manager": "yarn",
      "dependencies": [
        {
          "package": {
            "name": "babel-code-frame"
          },
          "version": "7.4.4"
        }
      ]
    }
  ]
        HERE
      end

      it "sorts dependency_files by dependency package.name in ascending order" do
        packages = subject.map { |f| f["dependencies"][0].dig("package", "name") }
        expect(packages).to eql %w[babel-code-frame @babel/code-frame @babel/core]
      end
    end

    context "with nil" do
      let(:input) { nil }

      specify { expect(subject).to eql [] }
    end
  end

  describe "dependency_file" do
    subject { described_class.dependency_file input }

    context "with empty hash" do
      let(:input) { {} }

      specify { expect { subject }.not_to raise_error }
    end
  end

  describe "dependencies" do
    subject { described_class.dependencies input }

    context "with npm dependencies" do
      let(:input) do
        JSON.parse <<-HERE
      [
        {
          "iid": 1,
          "package": {
            "name": "bbbb"
          },
          "version": "1.0"
        },
        {
          "iid": 2,
          "package": {
            "name": "aaaa"
          },
          "version": "3.0"
        },
        {
          "iid": 3,
          "package": {
            "name": "aaaa"
          },
          "version": "2.0"
        }
      ]
        HERE
      end

      it "sorts objects" do
        versions = subject.map { |d| d["version"] }
        expect(versions).to eql %w[2.0 3.0 1.0]
      end
    end

    context "with nil" do
      let(:input) { nil }

      specify { expect(subject).to eql [] }
    end
  end

  describe "dependency" do
    subject { described_class.dependency input }

    context "with nuget dependency" do
      let(:input) do
        JSON.parse <<-HERE
        {
          "iid": 84,
          "dependency_path": [
            {
              "iid": 49
            },
            {
              "iid": 58
            },
            {
              "iid": 82
            }
          ],
          "package": {
            "name": "Microsoft.NETCore.Platforms"
          },
          "version": "1.1.0"
        }
        HERE
      end

      it "strips variable fields" do
        expect(subject["iid"]).to be_nil
        expect(subject["dependency_path"].map { |dp| dp["iid"] }.compact).to be_empty
        expect(subject["dependency_path"].length).to be 3
      end

      it "keeps comparable fields" do
        expect(subject.dig("package", "name")).not_to be_empty
        expect(subject["version"]).not_to be_empty
      end
    end

    context "with empty hash" do
      let(:input) { {} }

      specify { expect { subject }.not_to raise_error }
    end
  end

  describe "remediations" do
    subject { described_class.remediations(input) }

    context "with remediations array" do
      let(:input) do
        JSON.parse <<-HERE
  [
    {
      "fixes": [
        {
          "cve": "yarn.lock:acorn:gemnasium:cd6cfe67-4650-49c6-a10a-ee4195a573fa",
          "id": "870edd191a813519639616849cdeb7adc726c6dea360d45c9d2ab8d77632b9e3"
        }
      ],
      "summary": "Upgrade acorn",
      "diff": "ZGlmZiAtLWdpdCBhL3lhcm4ubG9jayBiL3lhcm4ubG9jawppbmRleCA4OTU1MDZmLi44YWVjNzA2IDEwMDY0NAotLS0gYS95YXJuLmxvY2sKKysrIGIveWFybi5sb2NrCkBAIC0zLDUgKzMsNiBAQAogCiAKIGFjb3JuQF41LjMuMDoKLSAgdmVyc2lvbiAiNS43LjMiCi0gIHJlc29sdmVkICJodHRwczovL3JlZ2lzdHJ5Lnlhcm5wa2cuY29tL2Fjb3JuLy0vYWNvcm4tNS43LjMudGd6IzY3YWEyMzFiZjg4MTI5NzRiODUyMzVhOTY3NzFlYjZiZDA3ZWEyNzkiCisgIHZlcnNpb24gIjUuNy40IgorICByZXNvbHZlZCAiaHR0cHM6Ly9yZWdpc3RyeS55YXJucGtnLmNvbS9hY29ybi8tL2Fjb3JuLTUuNy40LnRneiMzZThkOGE5OTQ3ZDA1OTlhMTc5NmQxMDIyNWQ3NDMyZjRhNGFjZjVlIgorICBpbnRlZ3JpdHkgc2hhNTEyLTFEKytWRzdCaHJ0dlFwTmJCem92S05jMUZMR0dFRS9vR2U3Yjl4Sm0vUkZITUJlVWFVR3BsdVY5UkxqWmE0N1lGZFBjREFlbkVZdXE5cFFQY01kTEpnPT0K"
    },
    {
      "summary": "Upgrade the corn"
    },
    {
      "summary": "Upgrade a corn"
    }
  ]
        HERE
      end

      it "sorts remediations in by the summary field in ascending order" do
        summaries = subject.map { |r| r["summary"] }
        expect(summaries).to eql ["Upgrade acorn", "Upgrade a corn", "Upgrade the corn"]
      end
    end

    context "with nil" do
      let(:input) { nil }

      specify { expect(subject).to eql [] }
    end
  end

  describe "remediation" do
    subject { described_class.remediation input }

    context "with DS remediation" do
      let(:input) do
        JSON.parse <<-HERE
    {
      "fixes": [
        {
          "cve": "yarn.lock:acorn:gemnasium:cd6cfe67-4650-49c6-a10a-ee4195a573fa",
          "id": "870edd191a813519639616849cdeb7adc726c6dea360d45c9d2ab8d77632b9e3"
        }
      ],
      "summary": "Upgrade acorn",
      "diff": "ZGlmZiAtLWdpdCBhL3lhcm4ubG9jayBiL3lhcm4ubG9jawppbmRleCA4OTU1MDZmLi44YWVjNzA2IDEwMDY0NAotLS0gYS95YXJuLmxvY2sKKysrIGIveWFybi5sb2NrCkBAIC0zLDUgKzMsNiBAQAogCiAKIGFjb3JuQF41LjMuMDoKLSAgdmVyc2lvbiAiNS43LjMiCi0gIHJlc29sdmVkICJodHRwczovL3JlZ2lzdHJ5Lnlhcm5wa2cuY29tL2Fjb3JuLy0vYWNvcm4tNS43LjMudGd6IzY3YWEyMzFiZjg4MTI5NzRiODUyMzVhOTY3NzFlYjZiZDA3ZWEyNzkiCisgIHZlcnNpb24gIjUuNy40IgorICByZXNvbHZlZCAiaHR0cHM6Ly9yZWdpc3RyeS55YXJucGtnLmNvbS9hY29ybi8tL2Fjb3JuLTUuNy40LnRneiMzZThkOGE5OTQ3ZDA1OTlhMTc5NmQxMDIyNWQ3NDMyZjRhNGFjZjVlIgorICBpbnRlZ3JpdHkgc2hhNTEyLTFEKytWRzdCaHJ0dlFwTmJCem92S05jMUZMR0dFRS9vR2U3Yjl4Sm0vUkZITUJlVWFVR3BsdVY5UkxqWmE0N1lGZFBjREFlbkVZdXE5cFFQY01kTEpnPT0K"
    }
        HERE
      end

      it "strips variable fields" do
        expect(subject["fixes"].map { |f| f["id"] }.compact).to be_empty
      end
    end

    context "with empty hash" do
      let(:input) { {} }

      specify { expect { subject }.not_to raise_error }
    end
  end
end
