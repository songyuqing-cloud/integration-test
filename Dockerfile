# build security report schemas
FROM debian:buster-slim AS schemas

RUN apt-get update && apt-get install -y git
WORKDIR /build
COPY scripts/export_security-report-schemas_dist.sh /build/
RUN ./export_security-report-schemas_dist.sh ./dists

# build integration test image
FROM docker:20.10

RUN apk add --no-cache ruby ruby-bundler ruby-json ruby-bigdecimal git ruby-dev make gcc musl-dev

COPY --from=schemas /build/dists /usr/share/security-report-schemas
ENV SECURITY_REPORT_SCHEMAS_DIR="/usr/share/security-report-schemas"

COPY . /build
WORKDIR /build

RUN gem build *.gemspec
RUN gem install ./*.gem
RUN rspec

COPY scripts/dependency-scanning-qa.rb /usr/local/bin/dependency-scanning-qa
COPY scripts/secret-detection-qa.rb /usr/local/bin/secret-detection-qa
COPY scripts/sast-qa.rb /usr/local/bin/sast-qa
