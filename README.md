# Secure Analyzers Integration Test

Ruby gem, scripts and Docker image to implement integration tests for the Secure analyzer projects.

This is based on [RSpec](rspec.info) and RSpec [shared examples](https://relishapp.com/rspec/rspec-core/v/3-10/docs/example-groups/shared-examples).
