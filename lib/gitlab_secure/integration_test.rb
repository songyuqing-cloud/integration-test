# frozen_string_literal: true

require_relative "integration_test/version"

module GitlabSecure
  module IntegrationTest
    class Error < StandardError; end
  end
end
