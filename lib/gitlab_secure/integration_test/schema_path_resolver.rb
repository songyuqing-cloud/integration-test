# SchemaPathResolver returns the path to the schema
# that corresponds to a given security report.
#
module GitlabSecure
  module IntegrationTest
    class SchemaPathResolver
      attr_reader :report, :base_dir

      # Initialize a path resolver for a given report object.
      #
      # The report is the output of JSON.parse.
      # The optional base directory is where distributions
      # of the security report schemas are located.
      #
      # The base directory of the security report schema distributions
      # defaults to "security-report-schemas", and it can be configured using
      # the SECURITY_REPORT_SCHEMAS_DIR environment variable.
      #
      def initialize(report)
        @report = report
        @base_dir = ENV.fetch("SECURITY_REPORT_SCHEMAS_DIR", "security-report-schemas")
      end

      # Returns the path to the security report schema
      #
      def path
        File.join(base_dir, dist_dir, filename)
      end

      private

      def dist_dir
        "v" + report["version"]
      end

      def filename
        scan_type = report.dig("scan", "type")
        short_name = scan_type.sub("_", "-")
        "#{short_name}-report-format.json"
      end
    end
  end
end
