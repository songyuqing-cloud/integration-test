require "json"

# Comparable provides functions that convert objects of the GitLab Security Report,
# like the "scan" object, so that they can be compared.
#
# When the same project is scanned multiple times by the same scanner,
# the generated reports might be different, but the comparable objects remain exactly the same.
#
# The input objects are Ruby objects returned by JSON.parse.
# They are cloned and they remain unchanged.
#
module GitlabSecure
  module IntegrationTest
    module Comparable
      # Convert a scan hash so that it can be compared
      #
      def self.scan(scan_input)
        # do not touch the original scan object otherwise schema validation
        # will fail because of removed properties.
        # Marshal functions are used to perform a deep cloning;
        # the built-in clone method only performs a shallow cloning.
        clone(scan_input).tap do |scan|
          scan.delete("start_time")
          scan.delete("end_time")
          scan["scanner"]&.delete("version")
        end
      end

      # Convert a vulnerabilities array so that it can be compared
      #
      def self.vulnerabilities(vulns)
        (vulns || []).map { |v| vulnerability(v) }.sort_by { |x| JSON.generate(x) }
      end

      # Convert a vulnerability hash so that it can be compared
      #
      def self.vulnerability(vuln_input)
        clone(vuln_input).tap do |vuln|
          # drop .id
          vuln.delete("id")

          # create missing arrays
          vuln["links"] ||= []
          vuln["identifiers"] ||= []
          vuln["tracking"] ||= {}
          vuln["tracking"]["items"] ||= []

          # sort arrays using all comparable fields
          vuln["links"].sort_by! { |x| JSON.generate(x) }
          vuln["identifiers"].sort_by! { |x| JSON.generate(x) }

          # drop .location.dependency.iid
          vuln["location"]["dependency"].delete "iid" if vuln.dig("location", "dependency")

          # drop .location.commit.sha
          vuln["location"]["commit"].delete "sha" if vuln.dig("location", "commit")
        end
      end

      # Convert a dependency files array so that it can be compared
      #
      def self.dependency_files(files)
        (files || []).map { |f| dependency_file(f) }.sort_by { |f| JSON.generate(f) }
      end

      # Convert a dependency file hash so that it can be compared
      #
      def self.dependency_file(file_input)
        clone(file_input).tap do |file|
          file["dependencies"] = dependencies(file["dependencies"])
        end
      end

      # Convert a dependencies array so that it can be compared
      #
      def self.dependencies(deps)
        (deps || []).map { |dep| dependency(dep) }.sort_by { |x| JSON.generate(x) }
      end

      # Convert a dependency hash so that it can be compared
      #
      def self.dependency(dep_input)
        clone(dep_input).tap do |dep|
          # drop .iid
          dep.delete("iid")

          # create missing arrays
          dep["dependency_path"] ||= []

          # drop .dependency_path[].iid
          dep["dependency_path"].each do |path|
            path.delete("iid")
          end
        end
      end

      # Convert a remediations array so that it can be compared
      #
      def self.remediations(rems)
        (rems || []).map { |r| remediation(r) }.sort_by { |x| JSON.generate(x) }
      end

      # Convert a remediation hash so that it can be compared
      #
      def self.remediation(rem_input)
        clone(rem_input).tap do |rem|
          # create missing arrays
          rem["fixes"] ||= []

          # drop .fixes[].id
          rem["fixes"].each do |fix|
            fix.delete("id")
          end
        end
      end

      def self.clone(object)
        Marshal.load(Marshal.dump(object))
      end
      private_class_method :clone
    end
  end
end
