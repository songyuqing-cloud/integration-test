require 'json'
require 'English'

# DockerRunner is used to scan a target directory using the Docker image of the analyzer,
# and to collect the generated report as well as the analyzer exit code.
# It uses the docker CLI.
#
module GitlabSecure
  module IntegrationTest
    class DockerRunner
      SCRIPT_FILENAME = "run.sh".freeze

      # Result combines the output and exit code of the analyzer
      # with the path of the generated report,
      # and the JSON-decoded report if it exists.
      #
      Result = Struct.new(:report_path, :report, :exit_code, :combined_output)

      # Initialize cache
      @cache = {}

      # Run the analyzer image and scan a target directory
      # or return the result that's already in cache for the same arguments.
      #
      def self.run_with_cache(image_name, target_dir, **kwargs)
        key = kwargs.merge(image_name: image_name, target_dir: target_dir).to_s

        # return if already in cache
        return @cache[key] if @cache.key?(key)

        # scan target directory, update cache, and return
        @cache[key] = new(image_name, target_dir, **kwargs).run
      end

      attr_reader :image_name, :target_dir, :variables, :report_filename, :command, :script, :offline

      # Initialize a runner that scans a target directory with the given analyzer image.
      #
      # variables is a hash that defines environment variables
      # to be set when running the Docker container.
      #
      # command is an optional array that overrides the CMD defined in the Dockerfile.
      #
      # script is an optional Shell script to be executed in the container.
      # If defined, then this script is saved to the target directory
      # and executed in place of the default command.
      #
      # When offline is set the container runs without network connection.
      #
      def initialize(
        image_name, target_dir,
        variables: {}, report_filename: "gl-dependency-scanning-report.json",
        command: [], script: nil, offline: false)
        @image_name = image_name
        @target_dir = target_dir
        @variables = variables
        @report_filename = report_filename
        @command = command
        @script = script
        @offline = offline
      end

      # Run the analyzer image and return the result of the scan.
      #
      # Reports created by previous executions of the analyzer are removed before the scan.
      #
      def run
        remove_report
        docker_run
      end

      private

      def report_path
        File.join(target_dir, report_filename)
      end

      def report
        JSON.parse(File.read(report_path)) if File.exist?(report_path)
      end

      def remove_report
        FileUtils.rm(report_path) if File.exist?(report_path)
      end

      def has_script?
        !script.nil?
      end

      def script_path
        File.join(target_dir, SCRIPT_FILENAME)
      end

      def cmd_and_args
        # execute the Shell script if defined
        return File.join(".", SCRIPT_FILENAME) if has_script?

        # else execute given CMD and ARGS if defined
        command.join(" ")
      end

      def docker_run
        # save Shell script to target  dir if defined
        if has_script?
          File.open(script_path, File::CREAT | File::TRUNC | File::WRONLY, 0o0755) do |f|
            f.write(script)
          end
        end

        # run docker container, execute given command or Shell script
        mount_dir = "/app"
        opts = %w[-t --rm]
        opts << ["-v", "#{target_dir}:#{mount_dir}", "-w", mount_dir]
        opts << %w[--network none] if offline
        variables.each { |k, v| opts << ["--env", "#{k}=#{v}"] }
        combined_output = `docker run #{opts.join(" ")} #{image_name} #{cmd_and_args}`
        Result.new(report_path, report, $CHILD_STATUS.to_i, combined_output)
      end
    end
  end
end
