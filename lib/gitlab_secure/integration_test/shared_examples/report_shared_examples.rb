require "rspec"
require "json"
require "json-schema"

require_relative "../comparable"
require_relative "../schema_path_resolver"

RSpec.shared_examples "empty report" do
  describe "version" do
    specify { expect(report["version"]).not_to be_empty }
  end

  describe "scan" do
    specify { expect(report["scan"]).not_to be_empty }
  end

  describe "vulnerabilities" do
    specify { expect(report["vulnerabilities"]).to be_empty }
  end

  describe "dependency_files" do
    specify { expect(report["dependency_files"] || []).to be_empty }
  end
end

RSpec.shared_examples "non-empty report" do |scanned_files|
  describe "version" do
    specify { expect(report["version"]).not_to be_empty }
  end

  describe "scan" do
    specify { expect(report["scan"]).not_to be_empty }
  end

  describe "vulnerabilities" do
    specify { expect(report["vulnerabilities"]).not_to be_empty }
  end

  it "matches scanned files" do
    want = scanned_files.sort
    expect(report["vulnerabilities"].map { |v| v.dig("location", "file") }.uniq.sort).to eql want
    if report.dig("scan", "type") == "dependency_scanning"
      expect(report["dependency_files"].map { |df| df["path"] }.uniq.sort).to eql want
    end
  end
end

RSpec.shared_examples "recorded report" do
  describe "version" do
    subject { report["version"] }

    let(:expected) { recorded_report["version"] }

    specify { is_expected.to eql expected }
  end

  describe "scan" do
    subject { GitlabSecure::IntegrationTest::Comparable.scan(report["scan"]) }

    let(:expected) { GitlabSecure::IntegrationTest::Comparable.scan(recorded_report["scan"]) }

    it "is equivalent" do
      expect(subject).to eql expected
    end
  end

  describe "dependency_files" do
    subject { GitlabSecure::IntegrationTest::Comparable.dependency_files(report["dependency_files"]) }

    let(:expected) { GitlabSecure::IntegrationTest::Comparable.dependency_files(recorded_report["dependency_files"]) }

    it "is equivalent" do
      expect(subject).to eql expected
    end
  end

  describe "vulnerabilities" do
    subject { GitlabSecure::IntegrationTest::Comparable.vulnerabilities(report["vulnerabilities"]) }

    let(:expected) { GitlabSecure::IntegrationTest::Comparable.vulnerabilities(recorded_report["vulnerabilities"]) }

    it "is equivalent" do
      expect(subject).to eql expected
    end
  end

  describe "remediations" do
    subject { GitlabSecure::IntegrationTest::Comparable.remediations(report["remediations"]) }

    let(:expected) { GitlabSecure::IntegrationTest::Comparable.remediations(recorded_report["remediations"]) }

    it "is equivalent" do
      expect(subject).to eql expected
    end
  end
end

RSpec.shared_examples "valid report" do
  it "passes schema validation without errors" do
    # force schema to draft-04 because the security report schemas use draft-07
    # but it's not supported, making JSON::Validator fails with JSON::Schema::SchemaError
    # "Schema not found: http://json-schema.org/draft-07/schema#".
    schema = JSON.parse(File.read(GitlabSecure::IntegrationTest::SchemaPathResolver.new(report).path))
    schema["$schema"] = "http://json-schema.org/draft-04/schema#"

    # set dependency_files field to an empty array if not set
    # because it's required in Dependency Scanning report schema v3.0.0 and later.
    # See https://gitlab.com/gitlab-org/gitlab/-/issues/300877 for details
    report["dependency_files"] ||= []

    expect(JSON::Validator.fully_validate(
             schema, report, parse_data: false, errors_as_objects: true
           )).to eql([])
  end
end
