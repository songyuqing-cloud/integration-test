require "rspec"

RSpec.shared_examples "successful scan" do
  it "creates a report" do
    expect(File).to exist(scan.report_path)
  end

  describe "exit code" do
    specify { expect(scan.exit_code).to be 0 }
  end

  it "logs analyzer name and version" do
    expect(scan.combined_output).to match /\[INFO\].*analyzer v/
  end

  it "logs no error" do
    expect(scan.combined_output).not_to match /\[(ERRO|FATA)\]/
  end
end

RSpec.shared_examples "crashed scan" do
  it "creates no report" do
    expect(File).not_to exist(scan.report_path)
  end

  describe "exit code" do
    specify { expect(scan.exit_code).not_to be 0 }
  end
end

RSpec.shared_examples "failed scan" do
  it "creates no report" do
    expect(File).not_to exist(scan.report_path)
  end

  describe "exit code" do
    specify { expect(scan.exit_code).not_to be 0 }
  end

  it "logs an error" do
    expect(scan.combined_output).to match /\[(ERRO|FATA)\]/
  end
end
