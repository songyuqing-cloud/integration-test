#!/usr/bin/env rspec

require 'json'
require 'open-uri'

require 'gitlab_secure/integration_test/shared_examples/report_shared_examples'

actual_report = begin
                  default_filename = ENV.fetch('DEPENDENCY_SCANNING_REPORT', 'gl-dependency-scanning-report.json')
                  path = ENV.fetch('DS_REPORT_PATH', default_filename)
                  JSON.parse(File.read(path))
                rescue StandardError => e
                  raise "cannot load actual report: #{e}"
                end

expected_report = begin
                    default_filename = ENV.fetch('DEPENDENCY_SCANNING_REPORT', 'gl-dependency-scanning-report.json')
                    default_path = File.join('qa/expect', default_filename)
                    report_url = ENV['DS_REPORT_URL'] || ''
                    uri = report_url.empty? ? default_path : report_url
                    JSON.parse(URI.open(uri).read)
                  rescue StandardError => e
                    raise "cannot load expected report: #{e}"
                  end

context 'generated report' do
  let(:report) { actual_report }

  it_behaves_like 'recorded report' do
    let(:recorded_report) { expected_report }
  end

  it_behaves_like 'valid report'
end
