#!/usr/bin/env rspec

require 'json'
require 'open-uri'

require 'gitlab_secure/integration_test/shared_examples/report_shared_examples'

actual_report = begin
                  path = ENV.fetch('SECRET_DETECTION_REPORT_PATH', 'gl-secret-detection-report.json')
                  JSON.parse(File.read(path))
                rescue StandardError => e
                  raise "cannot load actual report: #{e}"
                end

expected_report = begin
                    JSON.parse(URI.open(ENV['SECRET_DETECTION_REPORT_URL']).read)
                  rescue StandardError => e
                    raise "cannot load expected report: #{e}"
                  end

context 'generated report' do
  let(:report) { actual_report }

  it_behaves_like 'recorded report' do
    let(:recorded_report) { expected_report }
  end

  it_behaves_like 'valid report'
end
